export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCPAESq_B8qCOyWGv9vP7mIlILVXCsFky8',
    authDomain: 'timetracking-f858e.firebaseapp.com',
    databaseURL: 'https://timetracking-f858e.firebaseio.com',
    projectId: 'timetracking-f858e',
    storageBucket: 'timetracking-f858e.appspot.com',
    messagingSenderId: '224991933744'
  }
};
