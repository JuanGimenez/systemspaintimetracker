// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCPAESq_B8qCOyWGv9vP7mIlILVXCsFky8',
    authDomain: 'timetracking-f858e.firebaseapp.com',
    databaseURL: 'https://timetracking-f858e.firebaseio.com',
    projectId: 'timetracking-f858e',
    storageBucket: 'timetracking-f858e.appspot.com',
    messagingSenderId: '224991933744',
    timestampsInSnapshots: true

  }
};
