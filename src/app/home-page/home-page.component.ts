import { Component, OnDestroy, OnInit } from '@angular/core';
import {Menu, MenuService} from '@rsm/rsm-angular-components';
import {
  APPLICATION_MENU,
  ExtendedMenu,
  MENU_EXAMPLE_1,
  MENU_EXAMPLE_2
} from '../app.commons';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  providers: []
})
export class HomePageComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  private modifier: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private menuService: MenuService
  ) {}

  ngOnInit() {
    this.subscription = this.route.params.subscribe(
      params => (this.modifier = params['modifier'] || null)
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  menuItems() {
    switch (this.modifier) {
      case 'example1':
        return [MENU_EXAMPLE_1];
      case 'example2':
        return [MENU_EXAMPLE_2];
      default:
        return APPLICATION_MENU.items;
    }
  }

  itemsOf(menu: Menu) {
    const items: Array<Menu> = [];
    this.menuService.forEveryItem(menu, item => {
      if (item !== menu && (<ExtendedMenu>item).path) {
        items.push(item);
      }
    });
    return items;
  }

  onSelect(item: Menu) {
    if ((<ExtendedMenu>item).path !== undefined) {
      this.router.navigate([(<ExtendedMenu>item).path]);
    }
  }
}
