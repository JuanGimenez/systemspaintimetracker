import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home/home-page.component';
import { TrackerPageComponent } from './tracker-page/tracker-page.component';
import { DayTrackerComponent } from './tracker-page/day-tracker/day-tracker.component';
import { ProjectTrackerComponent } from './tracker-page/project-tracker/project-tracker.component';
import { UsersListPageComponent, DialogDataDialog } from './admin/users-list-page/users-list-page.component';
import { ProjectsListPageComponent } from './admin/projects-list-page/projects-list-page.component';
import { CalendarPageComponent } from './calendar-page/calendar-page.component';
import { EditModalComponent } from './calendar-page/edit-modal/edit-modal.component';
import { DataUserComponent } from './user/data-user/data-user.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { EditUserPageComponent } from './user/edit-user-page/edit-user-page.component';
import { UserWeekListPageComponent } from './admin/user-week-list-page/user-week-list-page.component';
import { BrowserModule } from '@angular/platform-browser';
import { AngularFireModule } from '@angular/fire';

import { environment } from '../../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from '../services/auth.service';
import { NotifyService } from '../services/notify.service';
import { ComponentsRoutingModule } from './components-routing.module';
import { AdminGuardService } from '../services/adminguard.service';
import { AuthGuardService } from '../services/auth-guard.service';
import { LoginModule } from '../login/login.module';
import { UserDataComponent } from './shared/user-data/user-data.component';
import { DayComponent } from './shared/day/day.component';
import { AdminMainComponent } from './admin/admin-main/admin-main.component';
import { AbsenceComponent } from './absence/absence.component';
import { HolidaySicknessListComponent } from './user/holiday-sickness-list/holiday-sickness-list.component';
import { NgbModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { FlatpickrModule } from 'angularx-flatpickr';
import { DateAdapter, CalendarModule } from 'angular-calendar';
/*import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {  MAT_DATE_LOCALE,MAT_DATE_FORMATS } from "@angular/material";*/
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';


@NgModule({
  declarations: [
    HomePageComponent,
    TrackerPageComponent,
    DayTrackerComponent,
    ProjectTrackerComponent,
    UsersListPageComponent,
    ProjectsListPageComponent,
    CalendarPageComponent,
    EditModalComponent,
    UserDataComponent,
    DataUserComponent,
    NavbarComponent,
    AbsenceComponent,
    EditUserPageComponent,
    UserWeekListPageComponent,
    DialogDataDialog,
    AdminMainComponent,
    HolidaySicknessListComponent,
    DayComponent,
    AdminMainComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    ComponentsRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    LoginModule,
    HttpClientModule,
    NgbModule.forRoot(),
    NgbModalModule,
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
  ],
  entryComponents: [DialogDataDialog],
  providers: [
    AuthService,
    AuthGuardService,
    AdminGuardService,
    NotifyService,

  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ComponentsModule { }
