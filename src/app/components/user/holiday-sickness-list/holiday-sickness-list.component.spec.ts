import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HolidaySicknessListComponent } from './holiday-sickness-list.component';

describe('HolidaySicknessListComponent', () => {
  let component: HolidaySicknessListComponent;
  let fixture: ComponentFixture<HolidaySicknessListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HolidaySicknessListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HolidaySicknessListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
