import { Component } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { TrackingService } from '../../../services/tracking.service';
import { AngularFirestore } from '@angular/fire/firestore';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';

export class HoliSick {
  constructor(
      public beginDate: string,
      public endDate : string,
      public type : string,
      public workingDays : number,
      public status : string
  ) { }
}

export class DayData {
  constructor(
    public dayAbscence: any,
    public arrayAbscence: any
  ) { }
}

@Component({
  selector: 'app-holiday-sickness-list',
  templateUrl: './holiday-sickness-list.component.html',
  styleUrls: ['./holiday-sickness-list.component.css']
})
export class HolidaySicknessListComponent {
  
  myName = "absence list";
  dbT = this._angularFirestore.collection('Tracker');
  dbU = this._angularFirestore.collection('users');

  arrayHoli: any[] = [];
  holidays: any[] = [];
  arraySick: any[] = [];
  sicks: any[] = [];
  workingDays = 0;
  beginAbs: string;
  endAbs: string;
  status: string;
  dataAbscence: any[] = [];
  userDates: any[] = [];


  dataSource = new MatTableDataSource<any>();
  displayedColumns: string[] = ['type', 'beginDate', 'endDate', 'workingDays', 'status', 'button'];

  constructor(private _angularFirestore: AngularFirestore, 
              private _traking: TrackingService,
              private route:ActivatedRoute) {
                this.showData();
              }
            
  showData() {
    this.route.params.subscribe(parameters =>{ 
      const uid = parameters['id'];
    this.dbU.doc(uid).get().subscribe(dataUser => {
      this.userDates.push(dataUser.data());
      
    });
    this.dbT.doc(uid).collection('days').get().subscribe(dataDays => {
      dataDays.forEach(element => {
        // Sepero las vacaciones y las bajas en diferentes arrays
        if (element.data().extras.holiday !== 0) {
          this.holidays.push(element);
        } 
        if (element.data().extras.sick !== 0) {
          this.sicks.push(element);
        }
      });
      

      this.compareDate(this.holidays);
      this.holidays.forEach(item => {
        // console.log(item.id);
      });
      
      this.compareDate(this.sicks);
      this.correlativeAbsence(this.holidays, 'Holiday');
      this.correlativeAbsence(this.sicks, 'Sick');
      
    });
    
  });
    
  }

  private correlativeAbsence(abscences: any[], type: string) {
    // Variable para comprobar si las vacaciones son correlativas
    let dayAbs = moment(abscences[0].id, 'DDMMYYYY');

    this.beginAbs = dayAbs.format('DD.MM.YYYY');
    let onePush = true;
    for (let index = 0; index < abscences.length; index++) {
      // Si es finde o fiesta continúa la correlación de vacaciones
      while (this._traking.isWeekend(dayAbs) || this._traking.isBankHoliday(dayAbs)) {
        dayAbs = dayAbs.clone().add(1, 'd');
      }
      // Comprueba si los días de vacaciones NO son correlativos o HAY medio dia de vacaciones y hace push de ese periodo de vacaciones
      if (moment(abscences[index].id, 'DDMMYYYY').format('DDMMYYYY') !== dayAbs.format('DDMMYYYY') 
      ||  abscences[index].data().extras.holiday === 0.5){
        if (index === 0 && abscences[index].data().extras.holiday === 0.5) {
          this.pushData(abscences, type, index);
        } else {
          this.pushData(abscences, type, (index - 1));
        }
        dayAbs = moment(abscences[index].id, 'DDMMYYYY');
        this.beginAbs = moment(abscences[index].id, 'DDMMYYYY').format('DD.MM.YYYY');
      } 
      onePush = true;
      if (abscences[index].data().extras.holiday === 0.5) {
        this.workingDays = this.workingDays + 0.5; 
      } else {
        this.workingDays++;
      }
      // Para separar medios días de vacaciones al principio de un periodo
      if (abscences[index].data().extras.holiday === 0.5 &&
          moment(abscences[index].id, 'DDMMYYYY').format('DDMMYYYY') === dayAbs.format('DDMMYYYY')) 
        {
        this.pushData(abscences, type, index);
        this.beginAbs = moment(abscences[index].id, 'DDMMYYYY').format('DD.MM.YYYY');
        onePush = false;
      }
      // Hace push siempre que sea el ultimo dato del array menos cuando termina en medio día de vacaciones porque ya lo haría el de arriba
      if (((index + 1) === abscences.length && abscences[index].data().extras.sick === 1) ||
          ((index + 1) === abscences.length && abscences[index].data().extras.holiday !== 0.5)) {
        this.pushData(abscences, type, index);
      }
      dayAbs = dayAbs.clone().add(1, 'd');      
    }
    this.dataSource = new MatTableDataSource<HoliSick>(this.dataAbscence);
    // console.log(this.dataAbscence);
  }


  private pushData(abscences: any[], type: string, index: number) {
    if (abscences[index].data().extras.hStatus) {
      this.status = abscences[index].data().extras.hStatus;
    } else if (abscences[index].data().extras.sStatus) {
      this.status = abscences[index].data().extras.sStatus;
    }
    this.endAbs = moment(abscences[index].id, 'DDMMYYYY').format('DD.MM.YYYY');
    if (this.workingDays !== 0) {
      this.dataAbscence.push(new HoliSick(this.beginAbs, this.endAbs, type, this.workingDays, this.status));
    }
    this.workingDays = 0;
  }
  private compareDate(arrayDate: any[]) {
    arrayDate.sort((a: any, b: any) => {
      // tslint:disable-next-line:prefer-const
      let x = moment(a.id, 'DDMMYYYY').format('YYYYMMDD'); 
      // tslint:disable-next-line:prefer-const
      let y =  moment(b.id, 'DDMMYYYY').format('YYYYMMDD');
      
      return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
  }
}
