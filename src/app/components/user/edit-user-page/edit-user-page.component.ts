import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { AngularFireStorage } from '@angular/fire/storage';
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-edit-user-page',
  templateUrl: './edit-user-page.component.html',
  styleUrls: ['./edit-user-page.component.css']
})
export class EditUserPageComponent implements OnInit {

  storage = firebase.storage().ref();
  uid = this._afAuth.auth.currentUser.uid;
  public downloadURL;
  uploadProgress: any;
  user = {
    userName: '',
    email: '',
    telephone: 0,
    image: null
  };
  status: void;
  cantSave: boolean;
    
  constructor(private _userService: UserService, private _afAuth:AngularFireAuth,
              private afStorage: AngularFireStorage, private dialog:MatDialogRef<EditUserPageComponent>) {}

  ngOnInit() {
    this.updateImage();   
    this._userService.getUserData(this.uid).subscribe(userData => {
      this.user.userName = userData.data().displayName;
      this.user.email = userData.data().email;
      this.user.telephone = userData.data().phoneNumber;
      this.user.image = userData.data().photoURL;
    });
  }
  
  
  updateImage() {
    this.downloadURL = this._userService.getFileFromCloudStorage('images', this.uid);
    console.log(this.downloadURL);
    
  }

  saveUser(name) {
    this._userService.updateUser(this.uid, name);
    this.user.userName = name;
    
    this.dialog.close();
  }

  onCancel() {
    this.dialog.close();
  }
  
  upload(event) {
    const id = this.uid;
    const task = this._userService.uploadToCloudStorage(event, 'images', id);
    task.percentageChanges().subscribe(resolve => {
      this.cantSave = resolve !== 100 ? true : false;
    });
  }
  
}
