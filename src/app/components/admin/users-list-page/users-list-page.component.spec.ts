import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UsersListPageComponent} from './users-list-page.component';
import {CUSTOM_ELEMENTS_SCHEMA, InjectionToken} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';
import {MaterialModule} from '../../../material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularFirestore} from '@angular/fire/firestore';
import {AuthService} from '../../../services/auth.service';
import {of} from 'rxjs';

describe('UsersListPageComponent', () => {
  let component: UsersListPageComponent;
  let fixture: ComponentFixture<UsersListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      declarations: [UsersListPageComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {provide: AngularFirestore, useValue: mockFireStore}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


const mockFireStore = {
  collection: () => ({
    get: () => of([])
  })
};
