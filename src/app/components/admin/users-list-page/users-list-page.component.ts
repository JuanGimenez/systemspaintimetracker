import {SelectionModel} from '@angular/cdk/collections';
import {Component, Inject} from '@angular/core';
import {MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { AngularFirestore } from "@angular/fire/firestore";
import { UserService } from '../../../services/user.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { User } from '../../../model/user';
import {Router} from "@angular/router";
import {NotifyService} from "../../../services/notify.service";

export interface ProjectElement {
  displayName: string;
  email: number;
  uid: string;
  balance: number;
}

@Component({
  selector: 'app-users-list-page',
  templateUrl: './users-list-page.component.html',
  styleUrls: ['./users-list-page.component.css']
})
export class UsersListPageComponent {
  db = this._angularFirestore.collection('users');
  searchedProjects: any;
  users: any[];
  editable:any;
  displayedColumns: string[] = ['select', 'displayName', 'balance', 'role', 'holiday', 'AbsenceLink'];
  dataSource = new MatTableDataSource<ProjectElement>();
  selection = new SelectionModel<ProjectElement>(true, []);
  checked = false;

  months = new FormControl();
  monthList: string[] = ['January', 'February', 'March', 'April', 'May', 'June',
                          'July', 'August', 'September', 'October', 'November', 'December'];

  constructor(private _angularFirestore: AngularFirestore, public dialog: MatDialog, private _userService: UserService,
              private notify: NotifyService) {
    this.showProjects();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row =>{
          // console.log(row);
           this.selection.select(row);
          });
        
  }

  showProjects() {
    this.db.get().subscribe(data => {
      
      this.users = [];
      data.forEach((doc) => {
        this.users.push(doc.data());
        // console.log();
      });
      // console.log(this.users);
      this.dataSource = new MatTableDataSource<ProjectElement>(this.users);
      // console.log(this.dataSource);
      // console.log(this.users);
    });
  }

  selected() {
    console.log(this.selection.selected);
    
  }
  
  selectAdmin(id:string, roleAd:boolean) {
    console.log(id);

    this.db.doc(id).update({
      isAdmin: roleAd
    }).then(function() {
      console.log("Document successfully edited!");
    }).catch(function(error) {
        console.error("Error editting document: ", error);
    });
  }

  openHolidayDialog(holidays: number, id: string, nick: string, index:number): void {
      console.log(nick);
      console.log("index: " + index);
      const dialogRef = this.dialog.open(DialogDataDialog, {
        width: '250px',
        data: { holidays: holidays, displayName: nick }
        });

      dialogRef.beforeClosed().subscribe(result => {
        let isValid = false;
        if (result!=null){
          if (result<=50) {
            const pattern = /^([0-9]{1,2})(.[0-9]{1,2})?$/;
            const patternValidation = result.toString().match(pattern);
            if (patternValidation == null){
              isValid = false;
            } else {
              isValid = true;
            }
          }

        }

        if (!isValid){
          this.notify.update('Valid number required', 'error');
        } else {
          this.users[index].holidays = result;
          this._userService.saveHolidays(id, result);
          this.notify.update('Holidays updated', 'success');
        }
    });

    }
  
    changeAdmin(id:string, roleAd:boolean) {
      this._userService.selectAdmin(id, roleAd);
      }
  }

@Component({
  selector: 'app-holiday-dialog-data',
  templateUrl: 'holiday-dialog-data.html',
})
// tslint:disable-next-line:component-class-suffix
export class DialogDataDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogDataDialog>,
    @Inject(MAT_DIALOG_DATA) public data: User,
    private notify: NotifyService) {
  }

  isValidNumber() {
    let test = false;
    if (this.data.holidays != null){

      const pattern = /^([0-9]{1,2})(.[0-9]{1,2})?$/;
      const holidayString  = this.data.holidays.toString();
      const patternValidation = holidayString.match(pattern);
      if (patternValidation == null){
        test = false;
      } else {
        test = true;
      }
    }
    return test;
 }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

