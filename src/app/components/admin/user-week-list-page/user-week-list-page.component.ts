import { Component } from '@angular/core';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatTableDataSource } from '@angular/material';
import { TrackingService } from '../../../services/tracking.service';
import { Observable } from 'rxjs';

import {element} from 'protractor';


export class UserWeek {
  constructor(
      public weekNumber: number,
      public workedHours : number,
      public absence : number,
      public bankHolidays : number
  ) { }
}

export class WeekofUser {
  constructor(
      public weekNumber: number,
      public workedHours: number,
      public absence: number,
      public bankHolidays: number
  ) { }
}

@Component({
  selector: 'app-user-week-list-page',
  templateUrl: './user-week-list-page.component.html',
  styleUrls: ['./user-week-list-page.component.css']
})
export class UserWeekListPageComponent {

  db = this._angularFirestore.collection('Tracker');

  weekNumber: number;
  workedHours = 0;
  absence = 0;
  bankHolidays = 0;

  dayDates:any[] = [];
  arrayWeek: any[] = [];
  weekBankHolidays: any = [];
  dataSource = new MatTableDataSource<UserWeek>();
  displayedColumns: string[] = ['weekNumber', 'workedHours', 'absence', 'bankHolidays'];

  myId: any;

  public users: Observable<any[]>;

  constructor(private route:ActivatedRoute, private _angularFirestore: AngularFirestore, private _traking: TrackingService) {
    this.refreshData();
    this.users = _angularFirestore.collection('users').valueChanges();

   }

   refreshData() {
     let weekN: number;
     this.route.params.subscribe(parameters =>{
       const id = parameters['id'];
       this.myId = id;
       this.db.doc(id).collection('days').get().subscribe(data => {
         data.forEach(element => {
           if (element.data().startTime !== '') {
             this.dayDates.push(element.data());

           }
          });


          this.weekNumber = moment(this.dayDates[0].startTime).week();

          const startOfWeek = moment(this.dayDates[0].startTime).startOf('isoWeek');
          const endweek = moment().endOf('isoWeek');
          const dayToCheck = moment().add(1, 'week').format();
          let day = startOfWeek;

          while (day <= endweek) {
            if (!this._traking.isWeekend(day) && this._traking.isBankHoliday(day)) {

              this.weekBankHolidays.push(moment(day).week());

            }
            day = day.clone().add(1, 'd');
          }
          for (let index = 0; index < this.dayDates.length; index++) {
            
            weekN = moment(this.dayDates[index].startTime).week();
            if (this.weekNumber === weekN) {
              this.workedHours += this.dayDates[index].totalHours;
            } else {
              this.pushData();
              
              this.weekNumber = moment(this.dayDates[index].startTime).week();
              this.workedHours = this.dayDates[index].totalHours;
              this.absence = 0;
              this.bankHolidays = 0;
            }
            if ((index + 1) === this.dayDates.length) {
              this.pushData();
            }
            
          }
                 
         this.dataSource = new MatTableDataSource<UserWeek>(this.arrayWeek);

       });
     });
   }

   pushData(){
    this.weekBankHolidays.forEach(element => {
      if (element === this.weekNumber) {
        this.bankHolidays++;
      }
      });
      this.workedHours = Math.round(this.workedHours * 100) / 100;
      this.arrayWeek.push(new UserWeek(this.weekNumber, this.workedHours, 0, this.bankHolidays));

   }
}
