import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserWeekListPageComponent } from './user-week-list-page.component';
import {RouterTestingModule} from "@angular/router/testing";
import {MaterialModule} from "../../../material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {of} from "rxjs";
import {AngularFirestore} from "@angular/fire/firestore";

describe('UserWeekListPageComponent', () => {
  let component: UserWeekListPageComponent;
  let fixture: ComponentFixture<UserWeekListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        RouterTestingModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule
      ],
      declarations: [ UserWeekListPageComponent ],
      providers: [{provide: AngularFirestore, useValue: angularFirestoreMock}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserWeekListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

const angularFirestoreMock = {

    collection: () => ({
      doc: () => ({
        collection: () => ({
          get: ()=> of([])
        })
      })
    })
  };

