import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ProjectsListPageComponent } from './admin/projects-list-page/projects-list-page.component';
import { AdminGuardService } from '../services/adminguard.service';
import { UsersListPageComponent } from './admin/users-list-page/users-list-page.component';
import { AuthGuardService } from '../services/auth-guard.service';
import { HomePageComponent } from './home/home-page.component';
import { TrackerPageComponent } from './tracker-page/tracker-page.component';
import { DataUserComponent } from './user/data-user/data-user.component';
import { EditUserPageComponent } from './user/edit-user-page/edit-user-page.component';
import { UserWeekListPageComponent } from './admin/user-week-list-page/user-week-list-page.component';
import { AdminMainComponent } from './admin/admin-main/admin-main.component';
import {AbsenceComponent} from './absence/absence.component';
import { HolidaySicknessListComponent } from './user/holiday-sickness-list/holiday-sickness-list.component';
import {DayComponent} from './shared/day/day.component';

import { CalendarPageComponent } from './calendar-page/calendar-page.component';

const componentsRoutes: Routes = [
  { path: '', component: HomePageComponent, canActivate: [AuthGuardService],
    children: [
      { path: 'calendar', component: CalendarPageComponent, canActivate: [AuthGuardService] },
      { path: 'tracker', component: TrackerPageComponent, canActivate: [AuthGuardService] },
      { path: 'dataUser', component: DataUserComponent, canActivate: [AuthGuardService] },
      { path: 'editUser', component: EditUserPageComponent, canActivate: [AuthGuardService] },
      { path: 'absence', component: AbsenceComponent, canActivate: [AuthGuardService] },
      { path: 'absenceList/:id', component: HolidaySicknessListComponent, canActivate: [AuthGuardService] },
      { path: 'day', component: DayComponent, canActivate: [AuthGuardService] },
      { path: 'admin', component: AdminMainComponent, canActivate: [AdminGuardService],
        children: [
          { path: '', component: UsersListPageComponent, canActivate: [AdminGuardService] },
          { path: 'details/:id', component: UserWeekListPageComponent, canActivate: [AdminGuardService] },
          { path: 'projects', component: ProjectsListPageComponent, canActivate: [AdminGuardService] }
        ]
      }
    ]
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(componentsRoutes)
  ],
    exports: [RouterModule]
})
export class ComponentsRoutingModule { }
