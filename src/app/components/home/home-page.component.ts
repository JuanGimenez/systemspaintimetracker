import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { NotifyService } from '../../services/notify.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { MessagingService } from '../../services/messaging.service';
import { filter, take } from 'rxjs/operators';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  
  isLogged = false;
  events: string[] = [];
  opened = true;
  isAdmin = false;
  uid = this._afAuth.auth.currentUser.uid;

  constructor(private auth: AuthService, private router: Router, private notify: NotifyService,
              private _afAuth: AngularFireAuth, public msg: MessagingService)
  { }

  ngOnInit() {
    this.auth.user.subscribe(data => {

    })


    this.auth.isLogged().subscribe(auth => {
      if(auth) {
        this.isLogged = true;
      } else {
        this.isLogged = false;
      }
    });

    this.auth.user.subscribe(userFb => {
      if (userFb){
        this.isAdmin = userFb.isAdmin
        if(userFb.isAdmin){
          this.router.navigate(['/admin']);
        }else {
          this.router.navigate(['/tracker']);
        }
      }     
    });
    
    this.auth.user.pipe(
    filter(user => !!user)) // filter null
    .pipe(take(1)) // take first real user
    .subscribe(user => {
      if (user) {
        this.msg.getPermission(user);
        this.msg.monitorRefresh(user);
        this.msg.receiveMessages();
      }
    });
  

  }

  opening(){
    if(this.opened) {
      this.opened = false;
    } 
    else {
      this.opened = true;
    }
  }

}
