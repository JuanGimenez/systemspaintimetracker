import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TrackerPageComponent} from './tracker-page.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {of} from 'rxjs';

describe('TrackerPageComponent', () => {
  let component: TrackerPageComponent;
  let fixture: ComponentFixture<TrackerPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TrackerPageComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {provide: AuthService, useValue: mockAuthService},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
const mockAuthService = {
  isLogged: () => of(true)
};
