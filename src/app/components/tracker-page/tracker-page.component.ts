import {Component, Input, OnInit} from '@angular/core';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-tracker-page',
  templateUrl: './tracker-page.component.html',
  styleUrls: ['./tracker-page.component.css']
})
export class TrackerPageComponent implements OnInit {

  time = Date.now();
  myName = "tracker";

  constructor(private _auth:AuthService) { }

  ngOnInit() {
  }
  logout(){
    this._auth.signOut();
  }

}
