import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DayTrackerComponent } from './day-tracker.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {Observable, of} from 'rxjs';
import {TrackingService} from '../../../services/tracking.service';
import {RouterTestingModule} from '@angular/router/testing';
import {MaterialModule} from '../../../material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {AngularFireAuth} from "@angular/fire/auth";
import {UserService} from "../../../services/user.service";



describe('DayTrackerComponent', () => {
  let component: DayTrackerComponent;
  let fixture: ComponentFixture<DayTrackerComponent>;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        NoopAnimationsModule
      ],
      declarations: [ DayTrackerComponent ],
      providers: [ {provide: TrackingService, useValue: trackerServiceMock},
        {provide: AngularFireAuth, useValue: angularFireAuthMock},
        {provide: UserService, useValue: userServiceMock}],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();

    TestBed.compileComponents().catch(error => console.error(error));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DayTrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

const trackerServiceMock = {
  getActualTrackerInfo : () => of({
    exists: true,
    id: 'An ID',
    ref: null,
    metadata: null
  }),
  saveDay: d => undefined,
  calculatesTotalHours: (start,end,breakTime) => 100
};


const angularFireAuthMock = {
  auth:{
    currentUser:{
      uid:'jRAWu5kTPYTz1KEcqQ70OHYytYi2'
    }
  }
};

const userServiceMock = {
  getUserData: () => of( {data:()=> 0})
  // Mock for this in component this._userService.getUserData(this.uid).subscribe(
};



