import { Component, OnInit } from '@angular/core';
import { Tracking } from '../../../model/tracking';
import { TrackingService } from '../../../services/tracking.service';
import * as moment from 'moment';
import {AuthService} from '../../../services/auth.service';
import { Day } from '../../../model/day';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserService } from '../../../services/user.service';
@Component({
  selector: 'app-day-tracker',
  templateUrl: './day-tracker.component.html',
  styleUrls: ['./day-tracker.component.css']
})
export class DayTrackerComponent implements OnInit {
  timeStart: any;
  timeEnd: string;
  started = false;
  ended = false;
  breakFilled = true;
  break = 0;
  uid = this._afAuth.auth.currentUser.uid;
  balance;
  today = moment().format('DDMMYYYY');
  canSendBreak = false;
  bal:any;
protected dayObject : Day = {
  startTime: '',
  finishTime: '',
  break: null,
  totalHours: 0,
  extras: {
    holiday: 0,
    sick: 0,
    hStatus:'',
    sStatus:'',
    fileRef:'',

  },
  projects: { userProjects: {
    projectId: 0,
    hours: 0
  } }
};

protected tracking = {
  userId: '',
  days: this.dayObject
};

  constructor(private _trackingService: TrackingService, private _afAuth:AngularFireAuth,
    private _angularFirestore: AngularFirestore, private _userService: UserService,
    private auth:AuthService) {}

  ngOnInit() {

    this.balance = this._userService.getUserData(this.uid).subscribe( data => {
      this.balance = data.data().balance;
    });


    this._trackingService.getTrackerInfo(this.today).subscribe(tracking=>{
      
      if(tracking.data()){
        this.dayObject = tracking.data();
        if(tracking.data().startTime){
          this.timeStart = moment(this.dayObject.startTime).format('HH:mm');
          this.break = tracking.data().break;
        }
        if(tracking.data().finishTime){
          this.timeEnd = moment(this.dayObject.finishTime).format('HH:mm');
        }
        this.checkInit();
      }
      console.log(this.dayObject);
    });

    this._trackingService.currentBalance.subscribe(bal => this.bal = bal);



  }
  checkInit(){
    if (this.timeStart){
      this.started = true;
    }
    if(this.timeEnd){
      this.ended = true;
    }
    if(isNaN(this.break)){
      this.break = 0;
     }
     if ( !this.break ) {
      this.breakFilled = true;
    }
  }

  onStart() {
    this.timeStart = moment().format('HH:mm');
    this.dayObject.startTime = moment().format();
    this.tracking.days = this.dayObject;
    this._trackingService.saveDay(this.dayObject, this.today);
    this.started = true;
    this._trackingService.checkBeforeDay();
  }

  // tslint:disable:radix
  onEnd() {
    const time = moment();
    this.timeEnd = time.format('HH:mm');
    this.dayObject.finishTime = time.format();
    this.dayObject.totalHours = parseFloat(this._trackingService.calculatesTotalHours(this.dayObject.startTime,
    this.dayObject.finishTime, this.dayObject.break));


    this.ended = true;
    this._trackingService.calculateBalanceHours(this.dayObject.totalHours, this.today);
    this._trackingService.saveDay(this.dayObject, this.today);

  }

  // Add 5 minutes to the break
  onPlus() {
    if(this.break){
      this.break = this.break + 5;
    }else{
      this.break = 5;
    }
    this.validateBreak();

  }

  onMinus() {
    if (this.break !== this.dayObject.break) {
      this.break = this.break > 4 ? this.break - 5 : this.break ;
      this.validateBreak();
    }
  }
  
  onSetBreak(value: number) {
    if (value >= 0) {
        this.dayObject.break = value;
        this._trackingService.saveDay(this.dayObject, this.today);
        this.breakFilled = true;
    }
  }

  onKeyUp(value: any) {
    if (!value) {
      this.break = 0;
    } else {
      // tslint:disable-next-line:radix
      this.break = parseFloat(value);
    }
    this.validateBreak();
  }

  validateBreak() {
    if (this._trackingService.calculatesTotalHours(this.dayObject.startTime, moment().format(), this.break) > 0) {
      
      if (this.break !== this.dayObject.break && this.break !== 0) {
        this.canSendBreak = true;
      } else {
        this.canSendBreak = false;
      }
    } else {
      this.canSendBreak = false;
    }
  }
  logout(){
    this.auth.signOut();
  }
}
