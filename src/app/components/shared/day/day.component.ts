import { Component, OnInit } from '@angular/core';
import {TrackingService} from '../../../services/tracking.service';
import * as moment from 'moment';
import {log} from "util";
import {Day} from '../../../model/day';
import { NotifyService } from '../../../services/notify.service';
import { UserService } from '../../../services/user.service';
import { AngularFireAuth } from '@angular/fire/auth';


@Component({
  selector: 'app-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.css']
})
export class DayComponent implements OnInit {
  myName = 'edit day';
  daySearch: Date;
  newDay: any;
  total;
  startTimeModel: any;
  finishTimeModel: any;
  break = 0;
  defValue = '00:00';
  bal:any;
  uid = this._afAuth.auth.currentUser.uid;

  day: Day = {
    startTime: '',
    finishTime: '',
    break: null,
    totalHours: 0,
    extras: {
      holiday: 0,
      sick: 0,
      hStatus: '',
      sStatus: '',
      fileRef: '',

    },
    projects: {
      userProjects: {
        projectId: 0,
        hours: 0
      }
    }
  };

  constructor(public _trackingService: TrackingService, public notify : NotifyService,
     public _userService:UserService, private _afAuth: AngularFireAuth) {
  }

  ngOnInit() {
    this._trackingService.currentBalance.subscribe(bal => this.bal = bal);

  }

  // Search the day in the DB and fill out with the data
  searchDay() {
    this._trackingService.getTrackerInfo(moment(this.daySearch).format("DDMMYYYY")).subscribe(result => {
      if (result.data()) {
        // setear el dia
        this.total = moment(this.daySearch);
        this.startTimeModel = moment(result.data().startTime).format('HH:mm');

        if (moment(result.data().startTime).format('HH:mm') === "Invalid date") {
          this.startTimeModel = this.defValue;
        } else {
          this.startTimeModel = moment(result.data().startTime).format('HH:mm');
        }
        if (moment(result.data().finishTime).format('HH:mm') === "Invalid date") {
          this.finishTimeModel = this.defValue;
        } else {
          this.finishTimeModel = moment(result.data().finishTime).format('HH:mm');
        }
        this.day.break = result.data().break;
        this.newDay = moment(this.daySearch).format('DDMMYYYY');
      }
      else {
        const searchedDayFormat = moment(this.daySearch).format("DDMMYYYY");
        this._userService.getUserData(this.uid).subscribe(data => {
          const startDay = data.data().startDay;
          console.log(moment(searchedDayFormat, "DDMMYYYY").diff(moment(startDay, "DDMMYYYY")));
          if (moment(searchedDayFormat, "DDMMYYYY").diff(moment(startDay, "DDMMYYYY")) >= 0) {
            this.total = moment(this.daySearch);
            this.startTimeModel = this.defValue;
            this.finishTimeModel = this.defValue;
            this.newDay = moment(this.daySearch).format('DDMMYYYY');
          } else {
            this.notify.update('', 'success');
            
          }
        });
      }
    });
  }

  // Add 5 minutes to the break
  onPlus() {
    if (this.day.break) {
      this.day.break = this.day.break + 5;
    } else {
      this.day.break = 5;
    }

  }

  // Minus 5 min to the break
  onMinus() {
    if (this.day.break !== 0) {
      this.day.break = this.day.break > 4 ? this.day.break - 5 : this.day.break;

    }

  }

  // Sending to Update
  toSave() {

    if (((this.startTimeModel).length) === 4){
      this.startTimeModel = '0' + this.startTimeModel;
    }
    if (((this.finishTimeModel).length) === 4){
      this.finishTimeModel = '0' + this.finishTimeModel;
    }
    this.day.startTime = moment(this.total).format('YYYY' + '-' + 'MM' + '-' + 'DD' + 'T' + this.startTimeModel + ':00+01:00');
    this.day.finishTime = moment(this.total).format('YYYY' + '-' + 'MM' + '-' + 'DD' + 'T' + this.finishTimeModel + ':00+01:00');
    this.day.totalHours = parseFloat(this._trackingService.calculatesTotalHours(this.day.startTime,
      this.day.finishTime, this.day.break));

    if ( this.day.totalHours > this.break || this.newDay === (moment().format('DDMMYYYY'))) {
      if (this.startTimeModel !== this.defValue && this.finishTimeModel !== this.defValue && this.finishTimeModel !== "") {
        this._trackingService.calculateBalanceOnEdit(this.day.totalHours, this.newDay, this.day);

        this.notify.update('Succesfully saved', 'success');


      } else {
        this.day.finishTime = "";
        this._trackingService.saveDay(this.day, this.newDay);
        this.notify.update('Succesfully saved. This day is not finished, please come back to add the end time', 'success');

      }
    } else {
      this.notify.update('Could not be stored, please check the tracker', 'success');

    }
  }



}


 
