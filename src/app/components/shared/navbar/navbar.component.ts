import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { UserService } from '../../../services/user.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { MatDialog } from '@angular/material';
import { EditUserPageComponent } from '../../user/edit-user-page/edit-user-page.component';
import { TrackingService} from '../../../services/tracking.service';
import { User } from '../../../model/user';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  currentUser = this.afAuth.auth.currentUser.uid;
  profileName;
  profileImage;
  uid = this.afAuth.auth.currentUser.uid;
  dialogRef;
  userData: any = {
    balance: 0
  };
  showColorBalance = false;


  constructor(private auth: AuthService,
    private dialog: MatDialog,
    private db: AngularFirestore,
    private afAuth: AngularFireAuth,
    private _user: UserService,
    private _trackingService: TrackingService) {
    }
    
   ngOnInit() {
      this._user.getFileFromCloudStorage('images', this.uid).subscribe(data => {
        this.profileImage = data;
      }, err => {});

      this._user.getUserData(this.currentUser).subscribe(userData => {
        this.userData = userData.data();

        // @ts-ignore
       if (this.userData.balance > 0){
          // @ts-ignore
          this.userData.balance = '+' + this.userData.balance;
        }
        if (this.userData.balance < 0) {this.showColorBalance = true;} else {this.showColorBalance = false;}

      });

      this._trackingService.currentBalance.subscribe(bal => {
        this.userData.balance = bal;
        if (this.userData.balance < 0){this.showColorBalance = true;} else {this.showColorBalance = false;}

      });
  }

  logout() { 
    this.auth.signOut();
  }

  openEditDialog() {
    const dialogRef = this.dialog.open(EditUserPageComponent);

    dialogRef.afterClosed().subscribe(result => {
      this._user.getUserData(this.uid).subscribe(data => {
        this.userData.displayName = data.data().displayName;
      });
      this._user.getFileFromCloudStorage('images', this.uid).subscribe(data => {
        this.profileImage = data;
      });
    });
  }



}
