/* tslint:disable:quotemark */
import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from "@angular/fire/storage";
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
// import { DateAdapter, MAT_DATE_LOCALE,MAT_DATE_FORMATS } from "@angular/material";

import * as moment from 'moment';
import { NotifyService } from '../../services/notify.service';
import { TrackingService } from '../../services/tracking.service';
import { UserService } from '../../services/user.service';
// import { MessagingService } from 'src/app/services/messaging.service';

@Component({
  selector: 'app-absence',
  templateUrl: "./absence.component.html",
  styleUrls: ["./absence.component.css"],
  providers: []

})
export class AbsenceComponent implements OnInit {
  pageName = "absences";
  status = ["holiday", "sickness"];
  today = moment();
  task: any;
  minDate = moment("2018-10-04");
  halfDay = false;
  choosenStatus;
  downloadURL;
  valueStart;
  valueEnd;
  startDay;
  uid = this._afAuth.auth.currentUser.uid;
  pickerStart;

  constructor(

    private _tracking: TrackingService,
    private _notify: NotifyService,
  //   private _messaging: MessagingService,
    private _afStorage: AngularFireStorage,
    private userD: UserService,
    private _afAuth: AngularFireAuth
  ) {}

  ngOnInit() {
      console.log(this.minDate);

  }

  setAbsence() {
    this.userD
      .getUserData(this._afAuth.auth.currentUser.uid)
      .subscribe(userData => {
        let error = false;
        let failed = false;
        if (this.valueStart < userData.data().startTime) {
          this._notify.update("Please choose a starting day after you started in the company","info");
        } else if (
          this.choosenStatus === "sickness" &&
          this.valueStart > moment()
        ) {
          console.log(moment());
          console.log(this.valueStart);
          this._notify.update(
            "You cannot request sickness time in future",
            "info"
          );
        } else {
            const diff =
            parseInt(moment(this.valueEnd).format("DD")) -
            parseInt(moment(this.valueStart).format("DD"));
          if (!this.choosenStatus) {
            this._notify.update(
              "Please select the type of the absence",
              "info"
            );
            return;
          }
          if (
            !this.valueStart ||
            !this.valueEnd ||
            this.valueEnd < this.valueStart
          ) {
            this._notify.update(
              "Error setting holidays, Are you picking start and end days?",
              "info"
            );
            return;
          }
          for (let i = 0; i <= diff; i++) {
            const day = moment(this.valueStart)
              .add(i, "days")
              .format("DDMMYYYY");
            this._tracking.getTrackerInfo(day).subscribe(track => {
              console.log(track.data());
              if (track.data()) {
                if (
                  (track.data().extras.sick === 1 &&
                    this.choosenStatus === "holidays") ||
                  (track.data().extras.holiday === 1 &&
                    this.choosenStatus === "sickness") ||
                  (track.data().extras.holiday === 0.5 &&
                    this.choosenStatus === "sickness")
                ) {
                  this._notify.update(
                    "Error : you cannot set absence in a day that have already absence set",
                    "info"
                  );
                  error = true;
                  failed = true;

                  return;
                }
              }
              if (!this._tracking.isWeekend(moment(this.valueStart).add(i, "days")) &&
                  !this._tracking.isBankHoliday(moment(this.valueStart).add(i, "days")) &&
                  !error
              ) {
                console.log(this.choosenStatus);
                failed = this._tracking.setAbsence(
                  moment(this.valueStart)
                    .add(i, "days")
                    .format("DDMMYYYY"),
                  this.choosenStatus,
                  this.halfDay
                );
                if (i === diff && !failed && !error){
                  this._notify.update(
                    "Succesfully saved",
                    "info"
                  );
                  this.sendMsg(userData);
                }
              } else if ((this._tracking.isWeekend(moment(this.valueStart).add(i, "days")) ||
                        this._tracking.isBankHoliday(moment(this.valueStart).add(i, "days")) ||
                        error) && i === diff) {
                this._notify.update(
                  "Error saving absence, did you choose a bank holiday or weekend?",
                  "info"
                );
              }
            });
          }
        }
      });
  }

  sendMsg(userData) {
    this.userD.getAdminsUsers().subscribe(datos => {
      datos.docs.forEach(result =>{
        const message = {
          displayName: userData.data().displayName,
          type: this.choosenStatus,
          startDay: moment(this.valueStart).format("DD.MM.YYYY"),
          endDay: moment(this.valueEnd).format("DD.MM.YYYY"),
          uid: userData.data().uid,
          recipientId: result.id
        };
        
          // this._messaging.setMessage(message);
      }          
      );
     });
    
    
    
  }

  upload(event) {
    if (event.target.files[0]) {
      if (!this.valueStart) {
        this._notify.update("Please choose a starting day", "info");
        return;
      }
      const ref = this._afStorage.ref(
        "/sickness/" + this.uid + "/" + this.valueStart
      );
      this.task = ref.put(event.target.files[0]).then(() => {
        this.downloadURL = this._afStorage
          .ref("/sickness/" + this.uid + "/" + this.valueStart)
          .getDownloadURL();
      });
    }
  }
}
