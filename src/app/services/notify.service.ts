import { Injectable } from '@angular/core';

import { Subject } from 'rxjs';
import {MatSnackBar} from '@angular/material';

/// Notify users about errors and other helpful stuff
export interface Msg {
  content: string;
  style: string;
}

@Injectable()
export class NotifyService {
  constructor(private snackBar:MatSnackBar){}

  private _msgSource = new Subject<Msg | null>();

  msg = this._msgSource.asObservable();
  showNotification(msg:string,action:string){
    this.snackBar.open(msg,action,{duration:3000});
  }

  update(content: string, style: 'error' | 'info' | 'success') {
    this.showNotification(content,"Close");
    const msg: Msg = { content, style };
    this._msgSource.next(msg);
  }

  clear() {
    this._msgSource.next(null);
  }
}
