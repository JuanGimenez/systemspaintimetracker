import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import * as moment from 'moment';
import {AngularFireAuth} from '@angular/fire/auth';
import {Day} from '../model/day';
import {Observable} from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { UserService } from './user.service';
import {NotifyService} from './notify.service';
import {HOLIDAY} from '../../assets/json/holiday';
// import {log} from "util";


@Injectable({
  providedIn: 'root'
})
export class TrackingService {

  bankHoliday = HOLIDAY;

  protected dayObject : Day = {
    startTime: '',
    finishTime: '',
    break: null,
    totalHours: 0,
    extras: {
      holiday: 0,
      sick: 0,
      sStatus: '',
      hStatus: '',
      fileRef: ''
    },
    projects: {
      userProjects: {
        projectId: 0,
        hours: 0
      }
    }
  };


  calculatedBalance = 0;
  db = this._angularFirestore.collection('Tracker');
  uid = this._afAuth.auth.currentUser.uid;
  dbUser = this._angularFirestore.collection('users', ref => ref.where("uid","==",this.uid));
  today = moment().format('DDMMYYYY');
  day;
  n = 0;
  private newBalance = new BehaviorSubject('');
  currentBalance = this.newBalance.asObservable();
  totalTimeDay: any;
  bal: any = 0;

  constructor(private _auth: AuthService, private _angularFirestore: AngularFirestore,
  private _afAuth: AngularFireAuth, private _userService: UserService,
              private _notify: NotifyService) {

  }

  // Get the data of the day
  getTrackerInfo(day) : Observable<any> {
    return this.db.doc(this.uid).collection('days').doc(day).get();
  }
  changeBalance() {
    this.dbUser.doc(this.uid).get().subscribe(result => {
      if (result.data()) {
        this.bal = result.data().balance;
      }
      if (this.bal > 0){
        this.bal = "+" + this.bal;
      }
      this.newBalance.next(this.bal);
    });
  }

  saveDay(dayObject:Day, day){
    this.db.doc(this.uid).collection('days').doc(day).set(dayObject).
      then(()=>{
        console.log("Registrado correctamente");
        if (dayObject.finishTime !== '') {    this.changeBalance();  }
      },reason => {
        console.log(reason);
        }
      );

  }

  calculatesTotalHours(start:any, end:any, breakTime:number) {
    this.totalTimeDay = moment(end).diff(moment(start));
    this.totalTimeDay = Math.round((this.totalTimeDay/(3600*1000) - breakTime/60)*100) /100;

    return this.totalTimeDay;
  }

  calculateBalanceHours(totalHours: any, day) {
    totalHours = parseFloat(totalHours);

    this.getTrackerInfo(day).subscribe(data => {
      const formatDay = moment(day, "DDMMYYYY").format();
      if (!this.isWeekend(formatDay)) {
        
        if (data.data().extras.holiday === 0.5) {
          
          this.setBalance(totalHours, 4);
        } else if(data.data().extras.holiday === 0) {
          
          this.setBalance(totalHours, 8);
        }
      } else {
        this.setBalance(totalHours,  0);
      }
    });

    }

  calculateBalanceOnEdit(totalHours: any, newDay:string, day: any){
    totalHours = parseFloat(totalHours);

    this.getTrackerInfo(newDay).subscribe(data => {
      if (data.data()) {
        // enter when finishTime is void and is not today, add the total hours worked to the balance exists
        if (data.data().finishTime === "" && newDay !== this.today) {

          this._userService.getUserData(this.uid).subscribe(user => {

              let balance = user.data().balance;
              balance += totalHours;
              this.bal = balance;

              this.dbUser.doc(this.uid).update({
                balance : balance
              });
              this.saveDay(day, newDay);

          });

        }

        else {
          const dbTotalHours = data.data().totalHours;
          this._userService.getUserData(this.uid).subscribe(user => {
            let balance;
            // Do the balance, substract the hours expected
            if (newDay === this.today && data.data().finishTime === "") {
              // if have half day of holidays then the balance substract -4
              if (data.data().extras.holiday === 0.5 && data.data().extras.hStatus === "accepted"){
                balance = user.data().balance + (totalHours - 4);
                day.extras.holiday = 0.5;
                day.extras.hStatus = "accepted";

              }
              else {
                balance = user.data().balance + (totalHours - 8);
              }
            }
            // Actualize the balance, substracting to the totalHours the dbTotalHours.
            else {
              balance = user.data().balance + (totalHours - dbTotalHours);
            }

            balance = Math.round(balance * 100) / 100;
            this.bal = balance;
            this.dbUser.doc(this.uid).update({
              balance : balance
            });
            this.saveDay(day, newDay);
          });
        }

      } else {
        this._userService.getUserData(this.uid).subscribe(user => {
          let balance = user.data().balance;
          balance += totalHours;
          balance = Math.round(balance * 100) / 100;
          this.bal = balance;
          this.dbUser.doc(this.uid).update({
            balance : balance
          });
          this.saveDay(day, newDay);

        });
      }
    });
  }


  private setBalance(totalHoursDay: any, expectedHours: number) {

    let actualBalance = 0;
    this._userService.getUserData(this.uid).subscribe(userData => {
      if (userData.data().balance) {
        actualBalance = parseFloat(userData.data().balance);
      } else {
        actualBalance = 0;
      }
      let balance = actualBalance + totalHoursDay - expectedHours;
      balance = Math.round(balance * 100) / 100;
      this.bal = balance;

      this.dbUser.doc(this.uid).update({
        balance: balance
      }).then(() => {
        this.changeBalance();
      });
    });
    }


  checkBeforeDay() {
    this.n++;
    this.day = moment().add(-this.n, 'days').format('DDMMYYYY');

    const dayToCheck = moment().add(-this.n, 'days').format();

    this.getTrackerInfo(this.day).subscribe(data => {
      
      if (!data.data() || !data.data().finishTime) {
        this._userService.getUserData(this.uid).subscribe(userData => {
          const startDay = moment(userData.data().startDay, "DDMMYYYY").format();
          const actualDay = moment(this.day, "DDMMYYYY").format();
          
          if (moment(startDay).diff(moment(actualDay)) <= 0 ) {
            // If is not weekend and is not bank holiday
            if (!this.isWeekend(dayToCheck) && !this.isBankHoliday(dayToCheck)) {
              // If the day exists in the database
              if (data.data()) {
                
                this.calculatedBalance += this.isHoliday(data.data());

                if (!data.data().startTime) {
                  this.checkBeforeDay();
                }
              } else {
                this.calculatedBalance += 8;
                this.checkBeforeDay();
              }
            } else {
              this.checkBeforeDay();
            }
            } else {
            this.setBalance(0, this.calculatedBalance);

          }
        });

      } else {
        this.setBalance(0, this.calculatedBalance);
      }
      });

  }

  // Return the number of hours that the employee should work depending on the holidays
  isHoliday(dayContent) {
    if (dayContent.extras.holiday === 1 && dayContent.extras.hStatus === "accepted") {
      return 0;
    } else if(dayContent.extras.holiday === 0.5 && dayContent.extras.hStatus === "accepted") {
      return 4;
    } else {
      return 8;
    }
  }

  isWeekend(day){
    const dayOfWeek = moment(day).format('d');
    if ( dayOfWeek === "6" || dayOfWeek ===  "0") {
      return true;
    }else{
      return false;
    }
  }

  isBankHoliday(day){

    let bankHolidayDay = false;
    const dayFormat = moment(day).format('DD.MM.YY');
    const element = this.bankHoliday.bankHolidays;
    for (let index = 0; element.length > index; index++) {
      
      if (dayFormat === element[index].fecha) {
        bankHolidayDay = true;
      } 
    }
    if (bankHolidayDay) {
      return true;
    } else {
      return false;
    }
  }

  setAbsence(day,type,halfDay){
    this.dayObject  = {
      startTime: '',
      finishTime: '',
      break: null,
      totalHours: 0,
      extras: {
        holiday: 0,
        sick: 0,
        sStatus:'',
        hStatus:'',
        fileRef:''
      },
      projects: {
        userProjects: {
          projectId: 0,
          hours: 0
        }
      }
    };

  const statusPending = 'pending';
  const holidays = 'holidays';
  let error=false;
    if (type === holidays && halfDay){
      this.dayObject.extras.holiday = 0.5;
      this.dayObject.extras.hStatus = statusPending;
    }
    else if (type === holidays && !halfDay){
      this.dayObject.extras.holiday = 1;
      this.dayObject.extras.hStatus = statusPending;
    } else{
      this.dayObject.extras.sick = 1;
      this.dayObject.extras.sStatus = statusPending;

    }
    this.db.doc(this.uid).collection('days').doc(day).set(this.dayObject).catch(()=>{
      this._notify.update('Error saving','error');
      error = true;
      }
    );
    return error;
  }



}
