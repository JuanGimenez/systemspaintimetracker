import { Injectable } from '@angular/core';
import { AngularFirestore } from "@angular/fire/firestore";
import {Observable} from 'rxjs';
import { MatTableDataSource } from '@angular/material';
import { UserTracking } from '../model/user-tracking';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  db = this._angularFirestore.collection('users');
  
  constructor(private _angularFirestore:AngularFirestore, private afStorage: AngularFireStorage) {}

  getUserData(uid): Observable<any>;
  getUserData(uid: string) {
    return this.db.doc(uid).get();
  }

  selectAdmin(id:string, roleAd:boolean) {
    // console.log(roleAd);
    this.db.doc(id).update({
      isAdmin: roleAd
    }).then(function() {
      // console.log("Document successfully edited!");
    }).catch(function(error) {
      // console.error("Error editting document: ", error);
    });
  }

  updateUser(uid: string, name: string) {
    this.db.doc(uid).update({
      displayName: name
    });
  }
  saveHolidays(id: string, result: number) {
    this.db.doc(id).update({
      holidays: result
    }).then(function() {
      // console.log("Document successfully edited!");
    }).catch(function(error) {
      // console.error("Error editting document: ", error);
    });
  }

  uploadToCloudStorage(event, folder: string, uid: string) {

    const ref = this.afStorage.ref(folder).child(uid);
    return ref.put(event.target.files[0]);

  }

  getFileFromCloudStorage(folder:string, file: string) {
    return this.afStorage.ref(folder).child(file).getDownloadURL();
  }

  getAdminsUsers(){
    return this._angularFirestore.collection('users',ref=>ref.where('isAdmin','==',true)).get();
    
  }
  
}
