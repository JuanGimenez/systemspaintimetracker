import { TestBed } from '@angular/core/testing';
import { AuthGuardService } from './auth-guard.service';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {UserLoginComponent} from "../components/user/user-login/user-login.component";
import {AuthService} from "./auth.service";
import {NotifyService} from "./notify.service";
import {of} from "rxjs";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

describe('AuthGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      FormsModule,
      ReactiveFormsModule,
      RouterTestingModule.withRoutes([]),
      HttpClientTestingModule
    ],
    declarations: [ UserLoginComponent ],
    providers: [{provide: AuthService, useValue: mockAuthService},
      {provide: NotifyService, useValue: mockNotifyService}
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
  }));

  it('should be created', () => {
    const service: AuthGuardService = TestBed.get(AuthGuardService);
    expect(service).toBeTruthy();
  });
});

const mockAuthService = {
  isLogged: () => of(true)
};

const mockAuthGuardService = {
  canActivate: () => of(true)
};

const mockNotifyService = {};
