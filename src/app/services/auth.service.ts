import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';

import {auth, User, UserInfo} from 'firebase';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { NotifyService } from './notify.service';

import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { UserTracking } from '../model/user-tracking';
import { UserService } from './user.service';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable()
export class AuthService {
  user: Observable<UserTracking | null>;


  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    private notify: NotifyService,
    private _userService: UserService,
    private afStorage: AngularFireStorage
  ) {
    this.user = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<UserInfo>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
      // tap(user => localStorage.setItem('user', JSON.stringify(user))),
      // startWith(JSON.parse(localStorage.getItem('user')))
    );
  }


  //// Email/Password Auth ////

  emailSignUp(name: string, email: string, password: string, beginDay: string) {
    return this.afAuth.auth
      .createUserWithEmailAndPassword(email, password)
      .then(credential => {
        return this.setUserData( credential.user, beginDay, name ); // if using firestore
      })
      .catch(error => this.handleError(error,"Close"));
  }

  emailLogin(email: string, password: string) {
    return this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(credential => {
         return credential.user.email;
      })
      .catch(error => {
         this.handleError(error,"Close");
      });
  }

  // Sends email allowing user to reset password
  resetPassword(email: string) {
    const fbAuth = auth();

    return fbAuth
      .sendPasswordResetEmail(email)
      .then(() => this.notify.update('Reset requested. Please check your email to set new password', 'success'))
      .catch(error => this.handleError(error,"Close"));
  }

  signOut() {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(['/login']);
    });
  }
  userInfo(){
    return this.afAuth.auth;
  }

  isLogged() {
    return  this.user;
  }


  // If error, console log and notify user
  private handleError(error: Error,action:string) {
    this.notify.update(error.message, 'error');
  }


  // Sets user data to firestore after succesful login
  private setUserData(user: User, beginDay, name?) { //

    const userRef: AngularFirestoreDocument<UserTracking> = this.afs.doc(
      `users/${user.uid}`
    );

    const today = moment();
    const lastYearDay = moment().endOf('year');
    const pendingYearDays = lastYearDay.diff(beginDay, 'days') +1;
    const estimatedHolidays = (Math.round((pendingYearDays/365)*23* 100) / 100).toFixed(2);
    const dataUser: UserTracking = {
      uid: user.uid,
      displayName: user.displayName || name,
      email: user.email || null,
      photoURL: user.photoURL || 'https://goo.gl/Fz9nrQ',
      phoneNumber: user.phoneNumber || null,
      providerId: user.providerId || null,
      isAdmin :false,
      balance: 0,
      startDay: moment(beginDay).format('DDMMYYYY'),
      holidays: estimatedHolidays
    };
    
    return userRef.set(dataUser);
  }
}
