export interface Extras {
  holiday: number;
  hStatus:string;
  sick: number;
  sStatus:string;
  fileRef:string;
}
