import { Day } from "./day";

export interface Tracking {
    userId: string;
    days: Day;
}
