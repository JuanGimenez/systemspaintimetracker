import { UserProject } from "./userproject";
import { Extras } from './extras';

export interface Day {

    startTime: any;
    finishTime: any;
    break: number;
    totalHours: number;
    extras: Extras;
    projects: { userProjects: UserProject };

}
