import * as firebase from 'firebase';

export interface UserTracking extends firebase.UserInfo {
    isAdmin:boolean ;
    balance:number ;
    startDay:string;
    holidays:string;
}
