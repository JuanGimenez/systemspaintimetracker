export interface Project {
    projectId: number;
    projectName: string;
    totalHours: number;
}
