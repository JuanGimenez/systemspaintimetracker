export interface User {
    displayName: string;
    lastName: string;
    email: string;
    hoursBalance: number;
    role: string;
    holidays: number;
}
