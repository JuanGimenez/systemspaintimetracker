export interface UserProject {
    projectId: number;
    hours: number;
}
