import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HighlightJsModule } from 'angular2-highlight-js';

import { AppComponent } from './app.component';

import { RSMComponentsModule } from '@rsm/rsm-angular-components';

import { HomePageComponent } from './home-page/home-page.component';
import { ExamplePageComponent } from './example-page/example-page.component';

// Time Tracking Spain
import { ComponentsModule} from './components/components.module';
import { AppRoutingModule} from './app-routing.module';
import { AngularFireModule } from '@angular/fire';

const appRoutes: Routes = [
  { path: 'example-page', component: ExamplePageComponent },
  { path: 'home/:modifier', component: HomePageComponent },
  { path: '**', component: HomePageComponent }
];

@NgModule({
  declarations: [AppComponent, HomePageComponent, ExamplePageComponent],
  imports: [
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes, {
      enableTracing: false
    }),
    // TT Spain
    ComponentsModule,
    AppRoutingModule,
   // AngularFireModule.initializeApp(environment.firebase),
    HighlightJsModule,
    RSMComponentsModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
