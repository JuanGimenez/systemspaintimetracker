import {AfterContentInit, Component, HostBinding, OnInit, Renderer2} from '@angular/core';
import { Router } from '@angular/router';
import {
  ApplicationNavigationService,
  FooterService,
  Link,
  LINK_IMPRESSUM,
  MenuEvent,
  MenuSelectedEvent,
  MenuService,
  ProfileNavigationItem,
  ProfileNavigationService,
  ProfileService,
  RsmAppComponent,
  BackToTopService,
  ToastService,
  BrandingService,
  EmbeddingService,
  Brand,
  REWE_GROUP,
  REWE_SYSTEMS, AppContext,
} from '@rsm/rsm-angular-components';
import {
  APP_DEMO,
  APPLICATION_MENU,
  APPLICATIONS,
  ExtendedApplication,
  ExtendedMenu,
  LocalStorage,
  PROFILE_NAVIGATION_ITEMS
} from './app.commons';
import { AuthConfig, OAuthService } from 'angular-oauth2-oidc';

interface ExtendedProfileNavigationItem extends ProfileNavigationItem {
  path?: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent extends RsmAppComponent
  implements OnInit, AfterContentInit {
  // https://github.com/angular/angular-cli/issues/8434
  @LocalStorage persistedComponentCssClass?: Brand;

  private initialized = false;

  @HostBinding('class')
  componentCssClass = '';

  constructor(
    appContext: AppContext,
    renderer2: Renderer2,
  ) {
    super(
      appContext,
      renderer2,
    );
  }
  ngOnInit() {
    super.ngOnInit();

    this.menuService.menu = APPLICATION_MENU;
    this.backToTopService.visible = true;

    this.applicationNavigationService.applications = APPLICATIONS;
    this.applicationNavigationService.currentApplication = APP_DEMO;

    this.profileNavigationService.items = PROFILE_NAVIGATION_ITEMS;
    this.brandingService.brand = REWE_SYSTEMS;
  }

  ngAfterContentInit() {
    this.onBrandSwitched(this.persistedComponentCssClass || REWE_GROUP);
    this.initialized = true;
  }

  authConfig(): AuthConfig {
    return {
      issuer: 'https://gwba-stage.rewe-group.com/idp/oidc',
      clientId: 'rsm-ng-showcase-client2',
      dummyClientSecret: '123456789',
      loginUrl: 'https://gwba-stage.rewe-group.com/idp/oidc/authorize',
      tokenEndpoint: 'https://gwba-stage.rewe-group.com/idp/oidc/accessToken',
      userinfoEndpoint: 'https://gwba-stage.rewe-group.com/idp/oidc/profile',
      logoutUrl: 'https://gwba-stage.rewe-group.com/idp/logout'
    };
  }

  onBrandSwitched(brand: Brand) {
    super.onBrandSwitched(brand);
    this.persistedComponentCssClass = brand;
    this.signalEvent(`Brand ${brand.name} ' wurde angewendet.`);
  }

  onMenuItemSelected(menuEvent: MenuEvent) {
    if (menuEvent instanceof MenuSelectedEvent) {
      this.signalEvent(`Menüpunkt ${menuEvent.menu.label} wurde ausgewählt.`);
      if ((<ExtendedMenu>menuEvent.menu).path !== undefined) {
        this.router.navigate([(<ExtendedMenu>menuEvent.menu).path]);
      }
    }
  }

  onLinkSelected(link: Link) {
    if (link === LINK_IMPRESSUM) {
      this.router.navigate(['impressum']);
    }
  }

  onApplicationSelected(application: ExtendedApplication) {
    if (application.url) {
      window.open(application.url, '_external');
    } else {
      this.signalEvent(`Applikation ${application.label} wurde ausgewählt.`);
    }
  }

  onProfileNavigationItemSelected(item: ExtendedProfileNavigationItem) {
    if (item.path) {
      this.router.navigate([item.path]);
    } else {
      this.oauthService.refreshToken();
      this.signalEvent(`Profilemenüpunkt ${item.label} wurde angewendet.`);
    }
  }

  signalEvent(message: string) {
    if (!this.initialized) {
      return;
    }
    // this.toastService.info('Applikation: ' + message, 2000);
    console.log(`Applikation: ${message}`);
  }
}
