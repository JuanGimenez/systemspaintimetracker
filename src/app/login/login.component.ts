import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NotifyService } from '../services/notify.service';
import { AuthService } from '../services/auth.service';


type UserFields = 'displayName'|'email' | 'password';
type FormErrors = { [u in UserFields]: string };

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userForm: FormGroup;
  newUser = true; // to toggle login or signup form
  passReset = false; // set to true when password reset is triggered
  messageUser = '';
  now = new Date();
  beginDay;
  maxDate = new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate());
  minDate =  new Date(2018, 9, 4);
  formErrors: FormErrors = {
    'displayName': '',
    'email': '',
    'password': ''
  };

  validationMessages = {
    'email': {
      'required': 'Email is required.',
      'email': 'Email must be a valid email',
      'pattern': 'Email must be within rewe-group.com domain '
    },
    'password': {
      'required': 'Password is required.',
      'pattern': 'Password must be include at one letter and one number.',
      'minlength': 'Password must be at least 6 characters long.',
      'maxlength': 'Password cannot be more than 40 characters long.',
    },
  };

  constructor(private fb: FormBuilder, private auth: AuthService, 
              private router: Router, private notify: NotifyService) { }

  ngOnInit() {
    this.buildForm();
  }

  toggleForm() {
    this.newUser = !this.newUser;
    this.cleanerFieldsForm();
  }

  signup() {
    this.auth.emailSignUp(this.userForm.value['displayName'], 
                          this.userForm.value['email'], 
                          this.userForm.value['password'],
                          this.beginDay)
              .then(() => {
                this.messageUser = 'Welcome new user!';
                this.login(this.messageUser);
                this.cleanerFieldsForm();
              });
              
  }

  login( messageUser: string) {
    this.auth
        .emailLogin( this.userForm.value['email'], this.userForm.value['password'])
        .then((userRef) => {
          if(this.userForm.value['checkRememberMe'] === true) {
            localStorage.setItem('User', this.userForm.value['email']);
            localStorage.setItem('RememberMe', this.userForm.value['checkRememberMe']);
          }
          else {
            localStorage.removeItem('User');
            localStorage.removeItem('RememberMe');
          }
          this.router.navigate(['/']);
          this.notify.update(messageUser, 'success');
        });
  }

  resetPassword() {
    this.auth.resetPassword(this.userForm.value['email'])
      .then(() => this.passReset = true);
  }

  buildForm() {
    let userEmail = '';
    let rememberMe = false;
    this.messageUser = 'Welcome back!';
    if(JSON.parse(localStorage.getItem('RememberMe'))) {
      userEmail = localStorage.getItem('User');
      rememberMe = localStorage.getItem('RememberMe') === 'true';
    }
    this.userForm = this.fb.group({
      'displayName': ['',
        // Validators.required
      ],
      'email': [userEmail, [
        Validators.required,
        Validators.email,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+\\.)?[a-zA-Z]+\\.)?(rewe-group)\\.com$')
      ]],
      'password': ['', [
        Validators.minLength(6),
        Validators.maxLength(25),
      ]],
      'checkRememberMe' : rememberMe
    });

    this.userForm.valueChanges.subscribe((data) => this.onValueChanged(data));
    this.onValueChanged(); // reset validation messages
  }

  // Updates validation state on form changes.
  onValueChanged(data?: any) {
    this.passReset = false;
    if (!this.userForm) { return; }
    const form = this.userForm;
    for (const field in this.formErrors) {
      if (Object.prototype.hasOwnProperty.call
        (this.formErrors, field) && (field === 'displayName' || field === 'email' || field === 'password')) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if(field === 'email' && control.valid) {
          this.passReset = true;
        }
        if (control && control.dirty && !control.valid ) {
          const messages = this.validationMessages[field];
          if (control.errors) {
            for (const key in control.errors) {
              if (Object.prototype.hasOwnProperty.call(control.errors, key) ) {
                this.formErrors[field] += `${(messages as {[key: string]: string})[key]} `;
              }
            }
          }
        }
      }
    }
  }

  private cleanerFieldsForm() {
    const empty = '';
    this.userForm.value['displayName'].set(empty);
    this.userForm.value['email'].set(empty); 
    this.userForm.value['password'].set(empty);
  }

}
