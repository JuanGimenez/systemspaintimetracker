import { async, ComponentFixture, TestBed } from '@angular/core/testing';


import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';

import {of} from 'rxjs';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import { LoginComponent } from './login.component';
import { MaterialModule } from '../material';
import { AuthService } from '../services/auth.service';
import { AuthGuardService } from '../services/auth-guard.service';
import { NotifyService } from '../services/notify.service';


describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([]),
        MaterialModule,
        NoopAnimationsModule,
        RouterTestingModule,
        HttpClientTestingModule
  ],
      declarations: [ LoginComponent ],
      providers: [{provide: AuthService, useValue: mockAuthService},
        {provide: AuthGuardService, useValue: mockAuthGuardService},
        {provide: NotifyService, useValue: mockNotifyService}
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

const mockAuthService = {
  isLogged: () => of(true)
};

const mockAuthGuardService = {
  canActivate: () => of(true)
};

const mockNotifyService = {};




