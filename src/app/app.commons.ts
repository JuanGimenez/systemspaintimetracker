import {
  Application,
  Menu,
  MenuType,
  ProfileNavigationItem
} from '@rsm/rsm-angular-components';

export function LocalStorage(target: Object, decoratedPropertyName: string) {
  Object.defineProperty(target, decoratedPropertyName, {
    get: function() {
      const item = localStorage.getItem(decoratedPropertyName);
      return item === null ? null : JSON.parse(item);
    },
    set: function(value) {
      localStorage.setItem(decoratedPropertyName, JSON.stringify(value));
    }
  });
}

// ............................................................................

export interface ExtendedApplication extends Application {
  url: string;
}

export const APP_DEMO = {
  label: 'Angular Template'
};

export const APPLICATIONS = [
  APP_DEMO,
  {
    label: 'REWE UX',
    url: 'https://rdp.eil.risnet.de/display/REWEUX/REWE+Systems+UX'
  },
  {
    label: 'Showcase',
    url:
      'https://spa-rsm-angular-components-staging.kube-dev-central.koeln.ads.risnet.de/'
  },
  { label: 'Google', url: 'https://www.google.com/' }
] as Array<ExtendedApplication>;

// ............................................................................

export const PROFILE_NAVIGATION_ITEMS = [
  { icon: 'print', label: 'Drucken' },
  { icon: 'help', label: 'Hilfe und Handbuch' },
  { icon: 'schedule', label: 'Antwortzeiten' },
  {
    icon: 'settings',
    label: 'Einstellungen',
    path: 'navigation-demo'
  },
  {
    icon: 'person',
    label: 'Profil',
    path: 'profile-demo'
  }
] as Array<ProfileNavigationItem>;

// ............................................................................

export interface ExtendedMenu extends Menu {
  path?: string;
}

export const SELECTABLE_MENU = {
  label: 'Beispiel 2',
  path: 'example-page'
} as ExtendedMenu;

export const DISABLED_MENU = {
  label: 'N 4 (-enabled) und mit einem sehr langen Text',
  enabled: false,
  items: [
    {
      label: 'K 4.1'
    },
    {
      label: 'K 4.2 (-enabled)',
      enabled: false
    },
    {
      label: 'K 4.2'
    }
  ]
} as Menu;

export const MENU_EXAMPLE_1 = {
  label: 'Menu Beispiel 1',
  selectable: false,
  items: [
    {
      label: 'Beispiel 1',
      path: 'example-page'
    },
    SELECTABLE_MENU
  ]
};

export const MENU_EXAMPLE_2 = {
  label: 'Menu Beispiel 2',
  selectable: false,
  path: 'home/example2',
  items: [
    {
      label: 'Übermenu',
      selectable: false,
      items: [
        {
          label: 'Untermenu 1',
          path: 'example-page'
        } as ExtendedMenu,
        {
          label: 'Untermenu 2',
          path: 'example-page'
        }
      ]
    }
  ]
} as ExtendedMenu;

export let APPLICATION_MENU = {
  items: [MENU_EXAMPLE_1, MENU_EXAMPLE_2]
};

const APPLICATION_MENU2 = {
  items: [
    {
      label: 'N 1',
      items: [
        {
          label: 'K 1.1 (-enabled)',
          enabled: false
        },
        {
          label: 'K 1.2',
          items: [
            {
              label: 'P 1.2.1 (*)',
              enabled: false
            },
            {
              label: 'P 1.2.2 (*)',
              enabled: false
            },
            {
              label: 'P 1.2.3'
            }
          ]
        },
        {
          label: 'K 1.3'
        }
      ]
    },
    {
      label: 'N 2 (+Selectable)',
      selectable: true,
      items: [
        {
          label: 'K 2.1'
        },
        {
          label: 'K 2.2'
        },
        {
          label: 'K 2.3 (mit einem sehr langen Text)'
        }
      ]
    },
    {
      label: 'N 3 (no items)'
    },
    DISABLED_MENU,
    {
      label: 'N 5 (SideNav)',
      menuType: MenuType.sidebar,
      selectable: true,
      items: [
        {
          label: 'K 5.1',
          items: [
            {
              label: 'K 5.1.1'
            },
            {
              label: 'K 5.1.2'
            },
            {
              label: 'K 5.1.3'
            }
          ]
        },
        {
          label: 'K 5.2',
          items: [
            {
              label: 'K 5.2.1',
              items: [
                {
                  label: 'K 5.2.1.1',
                  items: [
                    {
                      label: 'K 5.2.1.1.1',
                      items: [
                        {
                          label: 'K 5.2.1.1.1.1'
                        },
                        {
                          label: 'K 5.2.1.1.1.2'
                        },
                        {
                          label: 'K 5.2.1.1.1.3'
                        },
                        {
                          label: 'K 5.2.1.1.1.4'
                        },
                        {
                          label: 'K 5.2.1.1.1.5'
                        }
                      ]
                    },
                    {
                      label: 'K 5.2.1.1.2'
                    },
                    {
                      label: 'K 5.2.1.1.3'
                    },
                    {
                      label: 'K 5.2.1.1.4'
                    },
                    {
                      label: 'K 5.2.1.1.5'
                    }
                  ]
                },
                {
                  label: 'K 5.2.1.2'
                },
                {
                  label: 'K 5.2.1.3'
                }
              ]
            },
            {
              label: 'K 5.2.2',
              enabled: false,
              items: [
                {
                  label: 'K 5.2.2.1'
                },
                {
                  label: 'K 5.2.2.2'
                },
                {
                  label: 'K 5.2.2.3'
                }
              ]
            },
            {
              label: 'K 5.2.3'
            }
          ]
        },
        {
          label: 'K 5.3 (mit einem extra langen Text)'
        }
      ]
    }
  ]
} as Menu;

if (window.location.href.includes('big-menu')) {
  if (APPLICATION_MENU2.items) {
    APPLICATION_MENU.items = APPLICATION_MENU.items.concat(
      APPLICATION_MENU2.items
    );
  }
}
