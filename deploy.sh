#!/bin/bash

CI_ENVIRONMENT_HOSTNAME="${CI_ENVIRONMENT_URL}"
CI_ENVIRONMENT_HOSTNAME="${CI_ENVIRONMENT_HOSTNAME/http:\/\//}"
CI_ENVIRONMENT_HOSTNAME="${CI_ENVIRONMENT_HOSTNAME/https:\/\//}"

export NAMESPACE=${CI_ENVIRONMENT_NAME}-${KUBE_NAMESPACE}

echo "CI_IMAGE                [${CI_IMAGE}]"
echo "CI_ENVIRONMENT_HOSTNAME [${CI_ENVIRONMENT_HOSTNAME}]"
echo "CI_ENVIRONMENT_NAME     [${CI_ENVIRONMENT_NAME}]"
echo "CI_ENVIRONMENT_SLUG     [${CI_ENVIRONMENT_SLUG}]"
echo "CI_ENVIRONMENT_URL      [${CI_ENVIRONMENT_URL}]"
echo "CI_PROJECT_NAME         [${CI_PROJECT_NAME}]"
echo "KUBE_NAMESPACE          [${KUBE_NAMESPACE}]"
echo "NAMESPACE               [${NAMESPACE}]"

echo "Forcing namespace ${NAMESPACE}..."

cat <<EOF | kubectl apply -f -
kind: Namespace
apiVersion: v1
metadata:
  name: ${NAMESPACE}
EOF

echo "Deploying ${CI_PROJECT_NAME} (replicas: 1) with ${CI_IMAGE}..."

cat <<EOF | kubectl apply -n ${NAMESPACE} --force -f -
kind: List
apiVersion: v1
items:

- kind: Deployment
  apiVersion: extensions/v1beta1
  metadata:
    name: ${CI_PROJECT_NAME}
    labels:
      app: $CI_ENVIRONMENT_SLUG
      pipeline_id: "$CI_PIPELINE_ID"
      build_id: "$CI_BUILD_ID"
  spec:
    replicas: 1
    template:
      metadata:
        labels:
          name: $CI_PROJECT_NAME
          app: $CI_ENVIRONMENT_SLUG
      spec:
        containers:
        - name: ${CI_PROJECT_NAME}-container
          image: ${CI_IMAGE}
          imagePullPolicy: Always
          env:
          - name: TZ
            value: "Europe/Berlin"
          ports:
          - name: bkrp-cnt-port
            containerPort: 80

- kind: Service
  apiVersion: v1
  metadata:
    name: ${CI_PROJECT_NAME}-service
    labels:
      app: $CI_ENVIRONMENT_SLUG
      pipeline_id: "$CI_PIPELINE_ID"
      build_id: "$CI_BUILD_ID"
  spec:
    selector:
      app: ${CI_ENVIRONMENT_SLUG}
    ports:
    - port: 80
      targetPort: bkrp-cnt-port

- kind: Ingress
  apiVersion: extensions/v1beta1
  metadata:
    name: ${CI_PROJECT_NAME}-ingress
    labels:
      app: $CI_ENVIRONMENT_SLUG
      pipeline_id: "$CI_PIPELINE_ID"
      build_id: "$CI_BUILD_ID"
    annotations:
      kubernetes.io/ingress.class: "nginx"
  spec:
    rules:
    - host: ${CI_ENVIRONMENT_HOSTNAME}
      http:
        paths:
        - path: /
          backend:
            serviceName: ${CI_PROJECT_NAME}-service
            servicePort: 80
EOF

echo "Waiting for deployment..."

kubectl rollout status -n "${NAMESPACE}" -w "deployment/${CI_PROJECT_NAME}"

echo "Application is accessible at: ${CI_ENVIRONMENT_URL}"
