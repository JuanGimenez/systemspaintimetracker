FROM nginx:latest

RUN mkdir /app
COPY dist /app

COPY nginx/default.conf /etc/nginx/conf.d/default.conf

CMD ["nginx", "-g", "daemon off;"]
